<?php

declare(strict_types=1);

namespace Gfi\HelloModule\Controller\Hello;


use Magento\Framework\App\Action\Action;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Result\Page;

class View extends Action
{

    public function execute()
    {
        /** @var Page $resultPage */
        $page = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        /** @var Template $resultPage */
        $block = $page->getLayout()->getBlock('gfi.hello.view');
        $block->setData('customer_name', 'okhachiai');

        return $page;
    }
}
