<?php


namespace Gfi\HelloModule\Controller\Hello;


use Magento\Framework\App\Action\Action;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultFactory;

class Index extends Action
{

    public function execute()
    {
        /** @var Json $result */
        $result = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $result->setData([
            "message" => "Hello World",
        ]);

        return $result;

    }
}
